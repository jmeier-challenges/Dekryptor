package view;

import jm.cipher.data.IAlphabetBinaryData;
import jm.cipher.data.IVariableBaseToIntData;
import jm.tools.Text;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.List;

/**
 * User: T05365A
 * Date: 30.07.12
 * Time: 13:24
 */
public class VariableBaseToIntDataPanel extends JPanel implements IVariableBaseToIntData {
    private JTextField basesTextField;

    public VariableBaseToIntDataPanel() {
        LayoutManager layout = new FlowLayout(FlowLayout.LEFT);
        this.setLayout(layout);

        this.add(new JLabel("Bases"));
        basesTextField = new JTextField("01", 40);
        this.add(basesTextField);
    }

    @Override
    public List<String> getBaseChars() {
        return Arrays.asList(basesTextField.getText().split(" "));
    }
}

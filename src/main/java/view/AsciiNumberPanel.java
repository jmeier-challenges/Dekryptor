package view;

import jm.cipher.data.IAsciiNumberData;

import javax.swing.*;
import java.awt.*;

/**
 * User: T05365A
 * Date: 30.07.12
 * Time: 14:17
 */
public class AsciiNumberPanel extends JPanel implements IAsciiNumberData {
    private JTextField digitsTextField;

    public String getFromDigits() {
        return digitsTextField.getText();
    }

    public AsciiNumberPanel() {
        LayoutManager layout = new FlowLayout(FlowLayout.LEFT);
        this.setLayout(layout);

        this.add(new JLabel("Digits"));
        digitsTextField = new JTextField("0123456789", 10);
        this.add(digitsTextField);

    }
}

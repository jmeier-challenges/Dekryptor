package view;

import jm.cipher.data.IAlphabetNumberData;
import jm.tools.Text;

import javax.swing.*;
import java.awt.*;

/**
 * User: T05365A
 * Date: 30.07.12
 * Time: 13:42
 */
public class AlphabetNumberPanel extends JPanel implements IAlphabetNumberData {
    private JTextField alphabetTextField;
    private JTextField fromDigitsTextField;

    public String getAlphabet() {
        return alphabetTextField.getText();
    }

    public String getFromDigits() {
        return fromDigitsTextField.getText();
    }

    public AlphabetNumberPanel() {
        LayoutManager layout = new FlowLayout(FlowLayout.LEFT);
        this.setLayout(layout);

        this.add(new JLabel("Alphabet"));
        alphabetTextField = new JTextField(Text.ALPHABET, 26);
        this.add(alphabetTextField);

        this.add(new JLabel("Digits"));
        fromDigitsTextField = new JTextField("0123456789", 10);
        this.add(fromDigitsTextField);
    }
}

package view;

import jm.cipher.data.IAlphabetBinaryData;
import jm.tools.Text;

import javax.swing.*;
import java.awt.*;

/**
 * User: T05365A
 * Date: 30.07.12
 * Time: 13:24
 */
public class AlphabetBinaryPanel extends JPanel implements IAlphabetBinaryData {
    private JTextField alphabetTextField;
    private JTextField bitLengthTextField;

    public String getAlphabet() {
        return alphabetTextField.getText();
    }

    public String getBitLength() {
        return bitLengthTextField.getText();
    }

    public AlphabetBinaryPanel() {
        LayoutManager layout = new FlowLayout(FlowLayout.LEFT);
        this.setLayout(layout);

        this.add(new JLabel("Alphabet"));
        alphabetTextField = new JTextField(Text.ALPHABET, 26);
        this.add(alphabetTextField);

        this.add(new JLabel("Length"));
        bitLengthTextField = new JTextField("7", 1);
        this.add(bitLengthTextField);
    }

}

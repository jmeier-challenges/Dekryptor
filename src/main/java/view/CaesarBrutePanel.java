package view;

import jm.cipher.data.ICaesarBruteData;
import jm.tools.Text;

import javax.swing.*;
import java.awt.*;

/**
 * User: T05365A
 * Date: 30.07.12
 * Time: 14:30
 */
public class CaesarBrutePanel extends JPanel implements ICaesarBruteData {
    private JTextField alphabetTextField;

    public String getAlphabet() {
        return alphabetTextField.getText();
    }

    public CaesarBrutePanel() {
        LayoutManager layout = new FlowLayout(FlowLayout.LEFT);
        this.setLayout(layout);

        this.add(new JLabel("Alphabet"));
        alphabetTextField = new JTextField(Text.ALPHABET, 20);
        this.add(alphabetTextField);
    }
}

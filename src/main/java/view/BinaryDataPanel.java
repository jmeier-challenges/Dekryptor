package view;

import jm.cipher.BinaryData;
import jm.cipher.data.IBinaryData;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;

/**
 * User: T05365A
 * Date: 03.08.12
 * Time: 09:28
 */
public class BinaryDataPanel extends JPanel implements IBinaryData {
    private JComboBox coderCombobox;
    @Override
    public String getCoder() {
        return coderCombobox.getSelectedItem().toString();
    }

    public BinaryDataPanel() {
        LayoutManager layout = new FlowLayout(FlowLayout.LEFT);
        this.setLayout(layout);

        Object[] coders = BinaryData.coders.keySet().toArray();
        Arrays.sort(coders);
        coderCombobox = new JComboBox(coders);
        this.add(coderCombobox);
    }
}

package view.panels;

import jm.cipher.data.IRsaData;
import jm.tools.Text;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * User: johann
 * Date: 13.10.12
 * Time: 19:10
 */
public class RsaPanel extends JPanel implements IRsaData {
    private JTextField pTextField;
    private JTextField qTextField;
    private JTextField eTextField;
    private JTextField nTextField;
    private JCheckBox privateCheckBox;
    private JComboBox alphabetComboBox;
    private JComboBox typeComboBox;

    private JPopupMenu popupMenu = new JPopupMenu();

    @Override
    public void setP(long p) {
        pTextField.setText(Long.toString(p));
    }

    @Override
    public long getP() {
        return parseLong(pTextField.getText());
    }

    @Override
    public void setQ(long q) {
        qTextField.setText(Long.toString(q));
    }

    @Override
    public long getQ() {
        return parseLong(qTextField.getText());
    }

    @Override
    public void setE(long e) {
        eTextField.setText(Long.toString(e));
    }

    @Override
    public long getE() {
        return parseLong(eTextField.getText());
    }

    @Override
    public void setN(long n) {
        nTextField.setText(Long.toString(n));
    }

    @Override
    public long getN() {
        return parseLong(nTextField.getText());
    }

    @Override
    public void setType(PT_TYPE type) {
        typeComboBox.setSelectedItem(type);
    }

    @Override
    public PT_TYPE getType() {
        return PT_TYPE.valueOf(typeComboBox.getSelectedItem().toString());
    }

    @Override
    public void setPrivate(boolean p) {
        privateCheckBox.setSelected(p);
    }

    @Override
    public boolean isPrivate() {
        return privateCheckBox.isSelected();
    }

    @Override
    public void setAlphabet(String alphabet) {
        alphabetComboBox.setSelectedItem(alphabet);
    }

    @Override
    public String getAlphabet() {
        return alphabetComboBox.getSelectedItem().toString();
    }

    private long parseLong(String l) {
        try {
            return Long.parseLong(l);
        } catch (NumberFormatException ex) {
            return -1L;
        }
    }

    public RsaPanel() {
        this.setLayout(new WrapLayout(WrapLayout.LEFT));

        this.add(new JLabel("p"));
        pTextField = new JTextField(5);
        this.add(pTextField);

        this.add(new JLabel("q"));
        qTextField = new JTextField(5);
        this.add(qTextField);

        this.add(new JLabel("e"));
        eTextField = new JTextField(5);
        this.add(eTextField);

        this.add(new JLabel("n"));
        nTextField = new JTextField(5);
        this.add(nTextField);

        Action action = new AbstractAction("p*q") {
            @Override
            public void actionPerformed(ActionEvent e) {
                nTextField.setText(String.valueOf(getP()*getQ()));
            }
        };
        popupMenu.add(action);
        nTextField.add(popupMenu);

        nTextField.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                if (e.isPopupTrigger())
                    popupMenu.show(e.getComponent(), e.getX(), e.getY());
            }

            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger())
                    popupMenu.show(e.getComponent(), e.getX(), e.getY());
            }
        });

        this.add(new JLabel("Private"));
        privateCheckBox = new JCheckBox();
        setPrivate(false);
        this.add(privateCheckBox);

        this.add(new JLabel("Type"));
        typeComboBox = new JComboBox(PT_TYPE.values());
        typeComboBox.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                alphabetComboBox.setEnabled(getType() == PT_TYPE.ALPHABET);
            }
        });
        this.add(typeComboBox);

        this.add(new JLabel("Alphabet"));
        alphabetComboBox = new JComboBox(Text.COMMON_ALPHABETS);
        alphabetComboBox.setEditable(true);
        alphabetComboBox.setEnabled(false);
        this.add(alphabetComboBox);
    }
}

package view.panels;

import jm.cipher.data.IAffineData;
import jm.tools.Text;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * User: johann
 * Date: 09.09.12
 * Time: 17:05
 */
public class AffinePanel extends JPanel implements IAffineData {

    private JTextField aTextField;
    private JTextField bTextField;
    private JTextField ivTextField;
    private JTextField alphabetTextField;
    private JCheckBox cbcCheckbox;

    @Override
    public void setA(int a) {
        aTextField.setText(Integer.toString(a));
    }

    @Override
    public int getA() {
        try {
            return Integer.parseInt(aTextField.getText());
        } catch (NumberFormatException ex) {
            return 1;
        }
    }

    @Override
    public void setB(int b) {
        bTextField.setText(Integer.toString(b));
    }

    @Override
    public int getB() {
        try {
            return Integer.parseInt(bTextField.getText());
        } catch (NumberFormatException ex) {
            return 1;
        }
    }

    @Override
    public void setPtAlphabet(String alphabet) {
        alphabetTextField.setText(alphabet);
    }

    @Override
    public String getPtAlphabet() {
        return alphabetTextField.getText();
    }

    @Override
    public void setCbc(boolean b) {
        cbcCheckbox.setSelected(b);
    }

    @Override
    public boolean isCbc() {
        return cbcCheckbox.isSelected();
    }

    @Override
    public void setIv(int iv) {
        ivTextField.setText(Integer.toString(iv));
    }

    @Override
    public int getIv() {
        try {
            return Integer.parseInt(ivTextField.getText());
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    public AffinePanel() {
        LayoutManager layout = new FlowLayout(FlowLayout.LEFT);
        this.setLayout(layout);

        this.add(new JLabel("a"));
        aTextField = new JTextField("1", 2);
        this.add(aTextField);

        this.add(new JLabel("b"));
        bTextField = new JTextField("1", 2);
        this.add(bTextField);

        this.add(new JLabel("Alphabet"));
        alphabetTextField = new JTextField(Text.LC_ALPHABET, 20);
        this.add(alphabetTextField);

        cbcCheckbox = new JCheckBox("CBC");
        cbcCheckbox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ivTextField.setEnabled(isCbc());
            }
        });

        this.add(cbcCheckbox);
        setCbc(false);

        this.add(new JLabel("IV"));
        ivTextField = new JTextField("", 2);
        ivTextField.setEnabled(false);
        this.add(ivTextField);

    }
}

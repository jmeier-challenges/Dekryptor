package view.panels;

import jm.cipher.ADFGX;
import jm.cipher.data.IADFGXData;

import javax.swing.*;
import java.awt.*;

/**
 * User: T05365A
 * Date: 01.10.12
 * Time: 14:43
 */
public class ADFGXPanel extends JPanel implements IADFGXData {
	private JComboBox ptAlphabetComboBox;
	private JTextField ctAlphabetTextField;
	private JTextField key1TextField;
	private JTextField key2TextField;

	@Override
	public void setKey1(String key1) {
		key1TextField.setText(key1);
	}

	@Override
	public String getKey1() {
		return key1TextField.getText();
	}

	@Override
	public void setKey2(String key2) {
		key2TextField.setText(key2);
	}

	@Override
	public String getKey2() {
		return key2TextField.getText();
	}

	@Override
	public void setPtAlphabet(String alphabet) {
		ptAlphabetComboBox.setSelectedItem(alphabet);
	}

	@Override
	public String getPtAlphabet() {
		return ptAlphabetComboBox.getSelectedItem().toString();
	}

	@Override
	public void setCtAlphabet(String alphabet) {
		ctAlphabetTextField.setText(alphabet);
	}

	@Override
	public String getCtAlphabet() {
		return ctAlphabetTextField.getText();
	}

	public ADFGXPanel() {
		LayoutManager layout = new FlowLayout(FlowLayout.LEFT);
		this.setLayout(layout);

		this.add(new JLabel("PT-Alphabet"));
		ptAlphabetComboBox = new JComboBox(ADFGX.COMMON_ALPHABETS);
        ptAlphabetComboBox.setEditable(true);
		this.add(ptAlphabetComboBox);

		this.add(new JLabel("CT-Alphabet"));
		ctAlphabetTextField = new JTextField("ADFGX", 5);
		this.add(ctAlphabetTextField);

		this.add(new JLabel("Key 1"));
		key1TextField = new JTextField(5);
		this.add(key1TextField);

		this.add(new JLabel("Key 2"));
		key2TextField = new JTextField(5);
		this.add(key2TextField);
	}
}

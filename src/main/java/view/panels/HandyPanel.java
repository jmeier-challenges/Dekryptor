package view.panels;

import jm.cipher.data.IHandyData;

import javax.swing.*;

/**
 * User: T05365A
 * Date: 06.11.12
 * Time: 11:22
 */
public class HandyPanel extends CipherBasePanel implements IHandyData {
	private JComboBox typeComboBox;

	@Override
	public HANDY_TYPE getType() {
		return HANDY_TYPE.valueOf(typeComboBox.getSelectedItem().toString());
	}

	@Override
	public void setType(HANDY_TYPE type) {
		typeComboBox.setSelectedItem(type.toString());
	}
	public HandyPanel() {

		this.add(new JLabel("Type"));
		typeComboBox = new JComboBox(HANDY_TYPE.values());
		typeComboBox.setSelectedItem(HANDY_TYPE.REPEATED_NUMBER);
		this.add(typeComboBox);
	}
}

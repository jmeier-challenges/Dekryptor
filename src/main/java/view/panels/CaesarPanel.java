package view.panels;

import jm.cipher.Caesar;
import jm.cipher.data.ICaesarData;
import jm.tools.Text;

import javax.swing.*;

/**
 * User: T05365A
 * Date: 30.07.12
 * Time: 09:33
 */
public class CaesarPanel extends CipherBasePanel implements ICaesarData {
	private JTextField keyTextField;
    private JComboBox alphabetComboBox;
    private JCheckBox preserveCase;


	@Override
	public void setKey(String key) {
		keyTextField.setText(key);
	}

	@Override
	public String getKey() {
		return keyTextField.getText();
	}

    @Override
    public void setAlphabet(String alphabet) {
        alphabetComboBox.setSelectedItem(alphabet);
    }

    @Override
    public String getAlphabet() {
        return alphabetComboBox.getSelectedItem().toString();
    }

    @Override
    public void setPreserveCase(boolean b) {
        preserveCase.setSelected(b);
    }

    @Override
    public boolean isPreserverCase() {
        return preserveCase.isSelected();
    }

    public CaesarPanel() {

	    this.add(new JLabel("Key"));
	    keyTextField = new JTextField("", 2);
	    this.add(keyTextField);

        this.add(new JLabel("Alphabet"));
        alphabetComboBox = new JComboBox(Caesar.CommonAlphabets);
        alphabetComboBox.setEditable(true);
        this.add(alphabetComboBox);

        this.add(new JLabel("Preserver Case"));
        preserveCase = new JCheckBox();
        this.add(preserveCase);

        setPreserveCase(true);
        setAlphabet(Text.LC_ALPHABET);

    }
}

package view.panels;

import jm.cipher.data.IHillData;
import jm.tools.Text;

import javax.swing.*;
import java.awt.*;

/**
 * User: johann
 * Date: 29.09.12
 * Time: 16:39
 */
public class HillPanel extends JPanel implements IHillData {
    private JTextField ptAlphabetTextField;
    private JTextField keyTextField;

    public HillPanel() {
        LayoutManager layout = new FlowLayout(FlowLayout.LEFT);
        this.setLayout(layout);

        this.add(new JLabel("PtAlphabet"));
        ptAlphabetTextField = new JTextField(Text.LC_ALPHABET, 20);
        this.add(ptAlphabetTextField);

        this.add(new JLabel("Key"));
        keyTextField = new JTextField(10);
        this.add(keyTextField);
    }

    @Override
    public String getPtAlphabet() {
        return ptAlphabetTextField.getText();
    }

    @Override
    public void setPtAlphabet(String alphabet) {
        ptAlphabetTextField.setText(alphabet);
    }

    @Override
    public String getKey() {
        return keyTextField.getText();
    }

    @Override
    public void setKey(String key) {
        keyTextField.setText(key);
    }

    @Override
    public void setKeyMatrix(long[][] keyMatrix) {
        // not yet implemented
    }

    @Override
    public long[][] getKeyMatrix() {
        // not yet implemented
        return new long[0][];
    }
}

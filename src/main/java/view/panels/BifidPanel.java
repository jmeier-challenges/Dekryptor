package view.panels;

import jm.cipher.Bifid;
import jm.cipher.data.IBifidData;

import javax.swing.*;
import java.awt.*;

/**
 * User: T05365A
 * Date: 02.10.12
 * Time: 11:49
 */
public class BifidPanel extends JPanel {
	private IBifidData data;

	private JComboBox ptAlphabetComboBox;
	private JTextField ctAlphabetTextField;
	private JTextField keyTextField;
	private JTextField nTextField;
	private JTextField equalCharsTextField;
	private JCheckBox reverseCheckBox;

	public BifidPanel() {
		LayoutManager layout = new FlowLayout(FlowLayout.LEFT);
		this.setLayout(layout);

		this.add(new JLabel("PT-Alphabet"));
		ptAlphabetComboBox = new JComboBox(Bifid.COMMON_ALPHABETS);
		ptAlphabetComboBox.setEditable(true);
		ptAlphabetComboBox.setSelectedIndex(0);
		this.add(ptAlphabetComboBox);

		this.add(new JLabel("CT-Alphabet"));
		ctAlphabetTextField = new JTextField("012345", 5);
		this.add(ctAlphabetTextField);

		this.add(new JLabel("Key"));
		keyTextField = new JTextField(5);
		this.add(keyTextField);

		this.add(new JLabel("Equal Chars"));
		equalCharsTextField = new JTextField("ji", 4);
		this.add(equalCharsTextField);

		this.add(new JLabel("N"));
		nTextField = new JTextField("5", 1);
		this.add(nTextField);

		this.add(new JLabel("Reverse"));
		reverseCheckBox = new JCheckBox();
		reverseCheckBox.setSelected(false);
		this.add(reverseCheckBox);
	}

	public void putData() {
		data.setCtAlphabet(ctAlphabetTextField.getText());
		data.setEqualChars(equalCharsTextField.getText());
		data.setKey(keyTextField.getText());
		data.setN(Integer.parseInt(nTextField.getText()));
		data.setPtAlphabet(ptAlphabetComboBox.getSelectedItem().toString());
		data.setReverse(reverseCheckBox.isSelected());
	}

	public void setData(Object data) {
		this.data = (IBifidData) data;
	}
}

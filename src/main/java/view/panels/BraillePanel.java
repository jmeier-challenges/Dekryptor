package view.panels;

import jm.cipher.data.IBrailleData;

import javax.swing.*;
import java.awt.*;

/**
 * User: johann
 * Date: 13.10.12
 * Time: 11:45
 */
public class BraillePanel extends JPanel implements IBrailleData {
    private JCheckBox binaryCheckBox;

    @Override
    public boolean isBinary() {
        return binaryCheckBox.isSelected();
    }

    @Override
    public void setBinary(boolean binary) {
        binaryCheckBox.setSelected(binary);
    }

    public BraillePanel() {
        LayoutManager layout = new FlowLayout(FlowLayout.LEFT);
        this.setLayout(layout);

        this.add(new JLabel("Binary"));
        binaryCheckBox = new JCheckBox();
        binaryCheckBox.setSelected(true);
        this.add(binaryCheckBox);
    }
}

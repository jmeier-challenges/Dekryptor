package view.panels;

import jm.cipher.data.IPolybiosData;

import javax.swing.*;
import java.awt.*;

/**
 * User: T05365A
 * Date: 01.10.12
 * Time: 12:00
 */
public class PolybiosPanel extends JPanel implements IPolybiosData {
	private JTextField ptAlphabetTextField;
	private JTextField ctAlphabetTextField;
	private JTextField keyTextField;
	private JTextField nTextField;
	private JTextField equalCharsTextField;
	private JCheckBox reverseCheckBox;

	@Override
	public void setKey(String key) {
		keyTextField.setText(key);
	}

	@Override
	public String getKey() {
		return keyTextField.getText();
	}

	@Override
	public void setPtAlphabet(String alphabet) {
		ptAlphabetTextField.setText(alphabet);
	}

	@Override
	public String getPtAlphabet() {
		return ptAlphabetTextField.getText();
	}

	@Override
	public void setN(int n) {
		nTextField.setText(Integer.toString(n));
	}

	@Override
	public int getN() {
		try {
			return Integer.parseInt(nTextField.getText());
		} catch (NumberFormatException ex) {
			return 5;
		}
	}

	@Override
	public void setCtAlphabet(String alphabet) {
		ctAlphabetTextField.setText(alphabet);
	}

	@Override
	public String getCtAlphabet() {
		return ctAlphabetTextField.getText();
	}

	@Override
	public void setReverse(boolean reverse) {
		reverseCheckBox.setSelected(reverse);
	}

	@Override
	public boolean isReverse() {
		return reverseCheckBox.isSelected();
	}

	@Override
	public void setEqualChars(String equalChars) {
		equalCharsTextField.setText(equalChars);
	}

	@Override
	public String getEqualChars() {
		return equalCharsTextField.getText();
	}

	public PolybiosPanel() {
		LayoutManager layout = new FlowLayout(FlowLayout.LEFT);
		this.setLayout(layout);

		this.add(new JLabel("PT-Alphabet"));
		ptAlphabetTextField = new JTextField("abcdefghiklmnopqrstuvwxyz", 20);
		this.add(ptAlphabetTextField);

		this.add(new JLabel("CT-Alphabet"));
		ctAlphabetTextField = new JTextField("123456", 5);
		this.add(ctAlphabetTextField);

		this.add(new JLabel("Key"));
		keyTextField = new JTextField(5);
		this.add(keyTextField);

		this.add(new JLabel("Equal Chars"));
		equalCharsTextField = new JTextField("ji", 4);
		this.add(equalCharsTextField);

		this.add(new JLabel("N"));
		nTextField = new JTextField("5", 1);
		this.add(nTextField);

		this.add(new JLabel("Reverse"));
		reverseCheckBox = new JCheckBox();
		reverseCheckBox.setSelected(false);
		this.add(reverseCheckBox);
	}
}

package view.panels;

import jm.cipher.Atbash;
import jm.cipher.data.IAtbashData;

import javax.swing.*;

/**
 * User: johann
 * Date: 17.10.12
 * Time: 15:15
 */
public class AtbashPanel extends CipherBasePanel implements IAtbashData {
    private JComboBox alphabetComboBox;
    @Override
    public void setAlphabet(String alphabet) {
        alphabetComboBox.setSelectedItem(alphabet);
    }

    @Override
    public String getAlphabet() {
        return alphabetComboBox.getSelectedItem().toString();
    }

    public AtbashPanel() {
        this.add(new JLabel("Alphabet"));
        alphabetComboBox = new JComboBox(Atbash.CommonAlphabets);
        alphabetComboBox.setEditable(true);
        this.add(alphabetComboBox);
    }
}

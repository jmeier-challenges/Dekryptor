package view;

import jm.cipher.data.INGramToAlphabetData;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * User: T05365A
 * Date: 07.08.12
 * Time: 10:20
 */
public class NGramToAlphabetPanel extends JPanel implements INGramToAlphabetData {
    private JTextField delimitersTextField;
    private JCheckBox newlineCheckBox;
    private JTextField nTextField;

    public NGramToAlphabetPanel() {
        LayoutManager layout = new FlowLayout(FlowLayout.LEFT);
        this.setLayout(layout);

        this.add(new JLabel("Delimiters"));
        delimitersTextField = new JTextField("", 3);
        delimitersTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                checkDelimiters();
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                checkDelimiters();
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                checkDelimiters();
            }

        });
        this.add(delimitersTextField);

        this.add(new JLabel("Newline Delimiter"));
        newlineCheckBox = new JCheckBox();
        newlineCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                checkDelimiters();
            }
        });
        this.add(newlineCheckBox);

        this.add(new JLabel("n"));
        nTextField = new JTextField("2", 2);
        nTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                checkN();
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                checkN();
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                checkN();
            }
        });
        this.add(nTextField);
    }

    private void checkN() {
        if (getN() > 0) {
            delimitersTextField.setText("");
            newlineCheckBox.setSelected(false);
        }
    }

    private void checkDelimiters() {
        if (!getDelimiters().isEmpty()) {
            nTextField.setText("");
        }
    }

    @Override
    public String getDelimiters() {
        return delimitersTextField.getText() + (newlineCheckBox.isSelected() ? "\n" : "");
    }

    @Override
    public int getN() {
        int n;
        try {
            n = Integer.parseInt(nTextField.getText());
        } catch (NumberFormatException ex) {
            n = 0;
        }
        return n;
    }

    @Override
    public boolean useDelimiters() {
        return getN() == 0;
    }
}

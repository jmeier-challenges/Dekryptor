package view;

import jm.tools.Text;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * User: johann
 * Date: 07.07.12
 * Time: 16:55
 */
public class CryptPanel extends JPanel implements IPtCt {
    private JPanel ptPanel = new JPanel();
    private JPanel ctPanel = new JPanel();
    private JTextArea ptTextArea = new JTextArea();
    private JTextArea ctTextArea = new JTextArea();
	private JPopupMenu popupMenu = new JPopupMenu();

    public CryptPanel() {
        LayoutManager layout = new GridLayout(2,0);
        this.setLayout(layout);

        ptPanel.setLayout(new BorderLayout());
        ptPanel.add(new JLabel("PT"), BorderLayout.WEST);
        ptPanel.add(new JScrollPane(ptTextArea), BorderLayout.CENTER);
        ptPanel.setBorder(new EmptyBorder(0, 0, 2, 0));
	    Action action = new AbstractAction("Wordsplit") {
		    @Override
		    public void actionPerformed(ActionEvent e) {
			    wordSplit(getValue("textfield"));
		    }
	    };
	    action.putValue("textfield", ptTextArea);

	    popupMenu.add(action);
	    ptTextArea.add(popupMenu);

	    ptTextArea.addMouseListener(new MouseAdapter() {
		    public void mousePressed(MouseEvent e) {
			    if (e.isPopupTrigger())
				    popupMenu.show(e.getComponent(), e.getX(), e.getY());
		    }

		    public void mouseReleased(MouseEvent e) {
			    if (e.isPopupTrigger())
				    popupMenu.show(e.getComponent(), e.getX(), e.getY());
		    }
	    });


	    this.add(ptPanel);

        ctPanel.setLayout(new BorderLayout());
        ctPanel.add(new JLabel("CT"), BorderLayout.WEST);
        ctPanel.add(new JScrollPane(ctTextArea), BorderLayout.CENTER);
        ctPanel.setBorder(new EmptyBorder(2, 0, 0, 0));

        this.add(ctPanel);
    }

	private void wordSplit(Object o) {
		String n = JOptionPane.showInputDialog(this, "Char count",
			"Char count",
			JOptionPane.PLAIN_MESSAGE);

		JTextArea area = (JTextArea) o;
		area.setText(Text.splitWords(area.getText(), Integer.parseInt(n)));
	}

	public String getPt() {
        return ptTextArea.getText();
    }

    public void setPt(String text) {
        ptTextArea.setText(text);
    }

    public String getCt() {
        return ctTextArea.getText();
    }

    public void setCt(String text) {
        ctTextArea.setText(text);
    }

}

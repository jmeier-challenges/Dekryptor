package view;

import jm.cipher.data.IAsciiBinaryData;

import javax.swing.*;
import java.awt.*;

/**
 * User: T05365A
 * Date: 30.07.12
 * Time: 13:51
 */
public class AsciiBinaryPanel extends JPanel implements IAsciiBinaryData {
    private JTextField bitLengthTextField;

    public String getBitLength() {
        return bitLengthTextField.getText();
    }

    @Override
    public void setBitLength(String bitLength) {
        bitLengthTextField.setText(bitLength);
    }

    public AsciiBinaryPanel() {
        LayoutManager layout = new FlowLayout(FlowLayout.LEFT);
        this.setLayout(layout);

        this.add(new JLabel("Bitlength"));
        bitLengthTextField = new JTextField("7", 1);
        this.add(bitLengthTextField);

    }
}

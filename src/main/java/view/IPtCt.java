package view;

/**
 * User: T05365A
 * Date: 07.08.12
 * Time: 11:48
 */
public interface IPtCt {
    String getCt();
    void setCt(String ct);

    String getPt();
    void setPt(String pt);
}

package view;

/**
 * User: johann
 * Date: 09.09.12
 * Time: 20:21
 */
public interface IBrutableData {
    String getAlphabet();
    String getKnownPlaintext();
}

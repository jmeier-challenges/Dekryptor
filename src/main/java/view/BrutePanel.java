package view;

import jm.cipher.IBrutableCipher;
import jm.cipher.brutable.*;
import jm.tools.Language;
import jm.tools.Text;
import jm.util.BestN;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * User: T05365A
 * Date: 24.08.12
 * Time: 16:11
 */
public class BrutePanel extends JPanel implements IBrutableData {
	private JList cipherJList = null;
    private JTextPane ctPane;
    private JTextPane ptPane;
    private JComboBox languageComboBox;
    private JTextField alphabetTextField;
    private JTextField knownPlainTextField;

    public  BrutePanel() {
		setLayout(new BorderLayout());
		createMenu();
        createTopPanel();
        createCenterPanel();
	}

	private void createMenu() {
		IBrutableCipher[] bruteableCiphers = {
			new CaesarBrute(),
			new ColumnarTranspositionBruter(),
			new RailfenceBruter(),
			new TrithemiusBruter(),
			new AtbashBruter(),
			new UECipher(),
			new VigenereBruter(),
            new AffineBrutable(),
            new HillBruter()
		};

		JPanel menuPanel = new JPanel(new BorderLayout());

		DefaultListModel model = new DefaultListModel();

		for (IBrutableCipher cipher : bruteableCiphers) {
			model.addElement(cipher);
		}

		cipherJList = new JList(model);
		menuPanel.add(cipherJList, BorderLayout.CENTER);

		add(menuPanel, BorderLayout.WEST);
	}

	private void createTopPanel() {
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JButton bruteButton = new JButton(new AbstractAction("Brute") {
			@Override
			public void actionPerformed(ActionEvent e) {
                try {
                    brute();
                } catch (Exception e1) {
                    //setPt(e1.getLocalizedMessage());
	                setPt(e1.getMessage());
                }
            }
		});

        languageComboBox = new JComboBox(Language.values());
        panel.add(languageComboBox);

        panel.add(bruteButton);

        panel.add(new JLabel("Alphabet"));
        alphabetTextField = new JTextField(Text.LC_ALPHABET, 26);
        panel.add(alphabetTextField);

        panel.add(new JLabel("Known plaintext"));
        knownPlainTextField = new JTextField(20);
        panel.add(knownPlainTextField);


        add(panel, BorderLayout.NORTH);
	}

	private void brute() throws Exception {
		Object[] values = cipherJList.getSelectedValues();
        BestN<Object> bestN = new BestN<Object>();
        for (Object value : values) {
            IBrutableCipher cipher = (IBrutableCipher) value;
            cipher.setCtAlphabet(getAlphabet());
            cipher.setKnownPlaintext(getKnownPlaintext());
            cipher.setLanguage(getLanguage());
            bestN.add(cipher.brute(getCt(), getLanguage()));
        }

        setPt(bestN.toString());
	}

    private void  createCenterPanel() {
        JPanel panel = new JPanel(new GridLayout(2, 0));

        ctPane = new JTextPane();
        ptPane = new JTextPane();

        panel.add(new JScrollPane(ctPane),0);
        ctPane.setBorder(new BevelBorder(BevelBorder.LOWERED));
        panel.add(new JScrollPane(ptPane), 1);
        ptPane.setBorder(new BevelBorder(BevelBorder.LOWERED));

        add(panel, BorderLayout.CENTER);
    }

    private void setPt(String pt) {
        ptPane.setText(pt);
    }

	private String getPt() {
		return ptPane.getText();
	}

    private String getCt() {
        return ctPane.getText();
    }

	private void setCt(String ct) {
		ctPane.setText(ct);
	}

    private Language getLanguage() {
        return (Language) languageComboBox.getSelectedItem();
    }

    @Override
    public String getAlphabet() {
        return alphabetTextField.getText();
    }

    @Override
    public String getKnownPlaintext() {
        return knownPlainTextField.getText();
    }

}

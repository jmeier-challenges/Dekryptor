package view.transposition;

import jm.cipher.transposition.data.IRailFenceData;

import javax.swing.*;
import java.awt.*;

/**
 * User: T05365A
 * Date: 21.08.12
 * Time: 16:20
 */
public class RailfencePanel extends JPanel implements IRailFenceData {
    private JTextField railCountTextField;
    private JTextField startRailTextField;
    @Override
    public void setRailCount(int count) {
        railCountTextField.setText(Integer.toString(count));
    }

    @Override
    public int getRailCount() {
        try {
            return Integer.parseInt(railCountTextField.getText());
        } catch (NumberFormatException ex) {
            return 2;
        }
    }

    @Override
    public void setStartRail(int rail) {
        startRailTextField.setText(Integer.toString(rail));
    }

    @Override
    public int getStartRail() {
        try {
            return Integer.parseInt(startRailTextField.getText());
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    public RailfencePanel() {
        LayoutManager layout = new FlowLayout(FlowLayout.LEFT);
        this.setLayout(layout);

        this.add(new JLabel("Rail count"));
        railCountTextField = new JTextField("2", 2);
        this.add(railCountTextField);

        this.add(new JLabel("Start rail"));
        startRailTextField = new JTextField("0", 2);
        this.add(startRailTextField);
    }
}

package view.transposition;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * User: T05365A
 * Date: 14.08.12
 * Time: 16:30
 *
 * http://www.nku.edu/~christensen/section%209%20transposition.pdf
 */
public class TranspositionSolverPanel extends JPanel {
    private JTable transpositonTable;
    //private String ct = "WfnSndoggceela hir";
    private String ctStart = "LITSM USEEH HEDEM EEWNE AENNI AMLNC CMFE\n";
    private int keyLength = 7;

    private JTextField keyLengthTextField;
    private JTextPane ctTextField;
    private JTextPane ptTextField;

    private JScrollPane scrollPane = null;

    private JCheckBox blockCipherCheckBox;


    public TranspositionSolverPanel() {
        setLayout(new BorderLayout());

        add(getDataPanel(), BorderLayout.NORTH);
        initTable();
    }

    private JPanel getDataPanel() {
        JPanel dataPanel = new JPanel();
        JPanel topPanel = new JPanel();
        topPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

        topPanel.add(new JLabel("Key length"));
        keyLengthTextField = new JTextField("4", 2);
        topPanel.add(keyLengthTextField, BorderLayout.NORTH);

        dataPanel.setLayout(new BorderLayout());
        ctTextField = new JTextPane();
        ctTextField.setText(ctStart);

        ptTextField = new JTextPane();
        ptTextField.setBackground(Color.lightGray);

        dataPanel.add(ctTextField, BorderLayout.CENTER);
        dataPanel.add(ptTextField, BorderLayout.SOUTH);
        dataPanel.add(topPanel, BorderLayout.NORTH);

        JButton startButton = new JButton(new AbstractAction("Start") {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    setKeyLength(Integer.parseInt(keyLengthTextField.getText()));
                    validate();
                } catch (NumberFormatException ex) {
                    // ignore
                }
            }
        });

        blockCipherCheckBox = new JCheckBox("Blockwise");
        blockCipherCheckBox.setSelected(false);

        topPanel.add(startButton);
        topPanel.add(blockCipherCheckBox);

        return dataPanel;
    }

    private Object[][] getRows() {
        int length = getCt().length();
        int rowCount = getRowCouunt();
        int colCount = getColCount();

        int completeCols = length % colCount;
        if (completeCols == 0) {
            completeCols = colCount;
        }

        String ct = getCt();

        Character[][] rows = new Character[rowCount][colCount];

        if (isBlockwise()) {
            int pos = 0;
            for (int row = 0; row < rowCount; row++) {
                for (int col = 0; col < colCount; col++) {
                    if (pos < ct.length()) {
                        rows[row][col] = ct.charAt(pos++);
                    }
                }
            }

        } else {
            int pos = 0;
            for (int col = 0; col < completeCols; col++) {
                for (int row = 0; row < rowCount; row++) {
                    rows[row][col] = ct.charAt(pos++);
                }
            }

            for (int col = completeCols; col < colCount; col++) {
                for (int row = 0; row < rowCount - 1; row++) {
                    if (pos < length) {
                        rows[row][col] = ct.charAt(pos++);
                    }
                }
            }
        }

        return rows;
    }

    private boolean isBlockwise() {
        return blockCipherCheckBox.isSelected();
    }

    private Object[] getHeaders() {
        int colCount = getColCount();
        String[] headers = new String[colCount];
        for (int col = 0; col < colCount; col++) {
            headers[col] = Integer.toString(col+1);
        }
        return headers;
    }

    private int getRowCouunt() {
        String ct = getCt();
        int rowCount = ct.length() / keyLength;
        if (ct.length() % keyLength != 0) {
            rowCount++;
        }
        return rowCount;
    }

    private int getColCount() {
        return keyLength;
    }

    private void setKeyLength(int length) {
        keyLength = length;
        initTable();
    }

    private void initTable() {
        if (scrollPane != null) {
            remove(scrollPane);
        }
        transpositonTable = new JTable(getRows(), getHeaders());
        transpositonTable.getColumnModel().addColumnModelListener(new TableColumnModelListener() {
            @Override
            public void columnAdded(TableColumnModelEvent e) {
            }

            @Override
            public void columnRemoved(TableColumnModelEvent e) {
            }

            @Override
            public void columnMoved(TableColumnModelEvent e) {
                updatePt();
            }

            @Override
            public void columnMarginChanged(ChangeEvent e) {
            }

            @Override
            public void columnSelectionChanged(ListSelectionEvent e) {
            }
        });

        scrollPane = new JScrollPane(transpositonTable);
        add(scrollPane, BorderLayout.WEST);
        validate();
    }

    private void updatePt() {
        StringBuilder pt = new StringBuilder();
        for (int row = 0; row < transpositonTable.getRowCount(); row++) {
            for (int col = 0; col < transpositonTable.getColumnCount(); col++) {
                pt.append(transpositonTable.getValueAt(row, col));
            }
        }

        ptTextField.setText(pt.toString());
    }

    public String getCt() {
        return ctTextField.getText();
    }

    public void setCt(String ct) {
        ctTextField.setText(ct);
    }


}

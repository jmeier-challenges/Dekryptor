package view;

import jm.cipher.data.IMorseData;

import javax.swing.*;
import java.awt.*;

/**
 * User: T05365A
 * Date: 27.07.12
 * Time: 10:57
 */
public class MorsePanel extends JPanel implements IMorseData{
    private JTextField ditTextField;
    private JTextField dahTextField;
    private JTextField spaceTextField;

    public String getDit() {
        String dit = ditTextField.getText();
        return dit;
    }

    public String getDah() {
        return dahTextField.getText();
    }

    public String getSpace() {
        return spaceTextField.getText();
    }

    public MorsePanel() {
        LayoutManager layout = new FlowLayout(FlowLayout.LEFT);
        this.setLayout(layout);

        this.add(new JLabel("Dit"));
        ditTextField = new JTextField(".", 1);
        this.add(ditTextField);
        this.add(new JLabel("Dah"));
        dahTextField = new JTextField("-", 1);
        this.add(dahTextField);
        spaceTextField = new JTextField(" ", 1);
        this.add(new JLabel("Space"));
        this.add(spaceTextField);
    }
}

package view;

import view.panels.WrapLayout;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: T05365A
 * Date: 28.09.12
 * Time: 14:04
 */
public class ScriptPanel extends JPanel {
	private JList scriptJList = null;
	private List<String> scripts = new ArrayList<String>();
	private Map<String, Icon> icons = new HashMap<String, Icon>();
	private JPanel topPanel;
	private JTextArea ptTextArea;

	public ScriptPanel() {
		setLayout(new BorderLayout());
		createMenu();
		createTopPanel();
		createCenterPanel();
	}

	private void createCenterPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		ptTextArea = new JTextArea("hello");
		panel.add(ptTextArea, BorderLayout.CENTER);

		add(panel, BorderLayout.CENTER);
	}

	private void createTopPanel() {
		topPanel = new JPanel(new WrapLayout(WrapLayout.LEFT));
		JScrollPane scroll = new JScrollPane(topPanel);

		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		add(scroll, BorderLayout.NORTH);
	}

    /*
	private List<String> getScripts() {
		CodeSource src = getClass().getProtectionDomain().getCodeSource();
		if (src != null) {
			URL jar = src.getLocation();
			System.out.println(jar);
			try {
				ZipInputStream zip = new ZipInputStream(jar.openStream());
				ZipEntry entry;
				while ((entry = zip.getNextEntry()) != null) {
				}
			} catch (IOException e) {
				e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
			}
			// Now examine the ZIP file entries to find those you care about.
		}
		else {
			// Fail...
		}


	}
    */

	private void createMenu() {

		ClassLoader loader = ClassLoader.getSystemClassLoader();
		java.net.URL url = loader.getResource("Schriften");
		System.out.println(url);
		if (url == null) {
			url = loader.getResource("/Schriften");
		}

		System.out.println(url);
		System.out.println();

		File dir;
		try {
			dir = new File(url.toURI());
		} catch (RuntimeException | URISyntaxException e) {
			return;
		}
		JPanel menuPanel = new JPanel(new BorderLayout());

		DefaultListModel<String> model = new DefaultListModel<>();

		for (File script : dir.listFiles()) {
			model.addElement(script.getName());
		}

		scriptJList = new JList<>(model);
		menuPanel.add(scriptJList, BorderLayout.CENTER);
		scriptJList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				scriptChanged(e.getFirstIndex());
			}
		});

		scriptJList.setBackground(Color.LIGHT_GRAY);

		add(menuPanel, BorderLayout.WEST);
	}

	private void scriptChanged(int firstIndex) {
		if (topPanel == null) {
			return;
		}
		// read icons
		String scriptName = "/Schriften/" + scriptJList.getSelectedValue();
		java.net.URL url = getClass().getResource(scriptName);
		File script;
		try {
			script = new File(url.toURI());
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}

		icons = new HashMap<String, Icon>();

		for (Component component : topPanel.getComponents()) {
			topPanel.remove(component);
		}

		for (File iconFile : script.listFiles()) {
			ImageIcon icon = createImageIcon(scriptName+"/"+iconFile.getName(), iconFile.getName());
			Action action = new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					addPt((String)getValue("value"));
				}
			};

			String nameWithoutExtension = getNameWithoutExtension(iconFile.getName());
			action.putValue(Action.SHORT_DESCRIPTION, nameWithoutExtension);
			action.putValue("value", nameWithoutExtension);

			JButton button = new JButton(action);
			button.setIcon(icon);
			button.setMargin(new Insets(0, 0, 0, 0));
			topPanel.add(button);
		}
		validate();
		repaint();

	}

	private void addPt(String value) {
		ptTextArea.setText(ptTextArea.getText() + value);
	}

	/** Returns an ImageIcon, or null if the path was invalid. */
	protected ImageIcon createImageIcon(String path,
	                                    String description) {
		java.net.URL imgURL = getClass().getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL, description);
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}

	private String getNameWithoutExtension(String filename) {
		int index = filename.lastIndexOf(".");
		if (index < 0) {
			return filename;
		} else {
			return filename.substring(0, index);
		}
	}


}

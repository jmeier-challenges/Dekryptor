package view;

import jm.cipher.Vigenere;
import jm.cipher.data.IVigenereData;

import javax.swing.*;
import java.awt.*;

/**
 * User: T05365A
 * Date: 30.07.12
 * Time: 16:09
 */
public class VigenerePanel extends JPanel implements IVigenereData {
    private JTextField keyTextField;
    private JComboBox alphabetComboBox;

    public String getKey() {
        return keyTextField.getText();
    }

	@Override
	public void setKey(String akey) {
		keyTextField.setToolTipText(akey);
	}

    @Override
    public void setAlphabet(String alphabet) {
        alphabetComboBox.setSelectedItem(alphabet);
    }

    @Override
    public String getAlphabet() {
        return alphabetComboBox.getSelectedItem().toString();
    }

    public VigenerePanel() {
        LayoutManager layout = new FlowLayout(FlowLayout.LEFT);
        this.setLayout(layout);

        this.add(new JLabel("Key"));
        keyTextField = new JTextField("", 10);
        this.add(keyTextField);

        this.add(new JLabel("Alphabet"));
        alphabetComboBox = new JComboBox(Vigenere.CommonAlphabets);
        alphabetComboBox.setEditable(true);
        this.add(alphabetComboBox);
    }
}

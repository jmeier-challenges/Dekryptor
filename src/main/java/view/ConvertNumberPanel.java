package view;

import jm.cipher.data.IConvertNumberData;

import javax.swing.*;
import java.awt.*;

/**
 * User: T05365A
 * Date: 10.08.12
 * Time: 14:28
 */
public class ConvertNumberPanel extends JPanel implements IConvertNumberData {
    private JTextField fromDigitsTextField;
    private JTextField toDigitsTextField;
    @Override
    public String getFromDigits() {
        return fromDigitsTextField.getText();
    }

    @Override
    public void setFromDigits(String digits) {
        fromDigitsTextField.setText(digits);
    }

    @Override
    public String getToDigits() {
        return toDigitsTextField.getText();
    }

    @Override
    public void setToDigits(String digits) {
        toDigitsTextField.setText(digits);
    }

    public ConvertNumberPanel() {

        LayoutManager layout = new FlowLayout(FlowLayout.LEFT);
        this.setLayout(layout);

        this.add(new JLabel("From digits"));
        fromDigitsTextField = new JTextField("0123456789", 20);
        this.add(fromDigitsTextField);

        this.add(new JLabel("To digits"));
        toDigitsTextField = new JTextField("0123456789abcdef", 20);
        this.add(toDigitsTextField);
    }
}

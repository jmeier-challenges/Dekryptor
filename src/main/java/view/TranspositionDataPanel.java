package view;

import jm.cipher.data.ITranspositionData;
import jm.cipher.transposition.ColumnarTransposition;

import javax.swing.*;
import java.awt.*;

/**
 * Date: 12.08.12
 * Time: 12:50
 */
public class TranspositionDataPanel extends JPanel implements ITranspositionData {
    private JTextField keyTextField;
    private JTextField keyLengthTextField;
    private JCheckBox blockwiseCheckBox;
    private JComboBox alphabetComboBox;

    @Override
    public void setKey(String key) {
        keyTextField.setText(key);
    }

    @Override
    public String getKey() {
        return keyTextField.getText();
    }

    @Override
    public void setKeyLength(int keyLength) {
        keyLengthTextField.setText(Integer.toString(keyLength));
    }

    @Override
    public int getKeyLength() {
        try {
            return Integer.parseInt(keyLengthTextField.getText());
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    @Override
    public void setBlockwise(boolean blockwise) {
        blockwiseCheckBox.setSelected(blockwise);
    }

    @Override
    public boolean isBlockwise() {
        return blockwiseCheckBox.isSelected();
    }

    @Override
    public void setAlphabet(String alphabet) {
        alphabetComboBox.setSelectedItem(alphabet);
    }

    @Override
    public String getAlphabet() {
        return alphabetComboBox.getSelectedItem().toString();
    }

    public TranspositionDataPanel() {
        LayoutManager layout = new FlowLayout(FlowLayout.LEFT);
        this.setLayout(layout);

        this.add(new JLabel("Key"));
        keyTextField = new JTextField("", 8);
        this.add(keyTextField);

        this.add(new JLabel("Keylength"));
        keyLengthTextField = new JTextField("", 2);
        this.add(keyLengthTextField);

        blockwiseCheckBox = new JCheckBox("Blockwise");
        this.add(blockwiseCheckBox);

        this.add(new JLabel("Alphabet"));
        alphabetComboBox = new JComboBox(ColumnarTransposition.CommonAlphabets);
        alphabetComboBox.setEditable(true);
        this.add(alphabetComboBox);
    }
}

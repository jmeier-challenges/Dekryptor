package view;

import jm.cipher.data.IPlayfairData;

import javax.swing.*;
import java.awt.*;

/**
 * User: T05365A
 * Date: 30.07.12
 * Time: 15:56
 */
public class PlayfairPanel extends JPanel implements IPlayfairData {
    private JTextField keyTextField;

    public String getKey() {
        return keyTextField.getText();
    }

    public PlayfairPanel() {
        LayoutManager layout = new FlowLayout(FlowLayout.LEFT);
        this.setLayout(layout);

        this.add(new JLabel("Key"));
        keyTextField = new JTextField("", 10);
        this.add(keyTextField);
    }
}

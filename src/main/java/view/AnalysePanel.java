package view;

import analyse.TextAnalyze;
import jm.cipher.analysis.KeyCount;
import jm.cipher.analysis.TextAnalysis;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * User: T05365A
 * Date: 31.07.12
 * Time: 11:01
 */
public class AnalysePanel extends JPanel {
    private JPanel ptPanel = new JPanel();
    private JPanel ctPanel = new JPanel();
    private JTextArea ptTextArea = new JTextArea();
    private JTextArea ctTextArea = new JTextArea();
    private TextAnalyze analyze = new TextAnalyze();
    private FrequencyTableModel ptModel = new FrequencyTableModel();
    private FrequencyTableModel ctModel = new FrequencyTableModel();

    public AnalysePanel() {
        LayoutManager layout = new GridLayout(2,0);
        this.setLayout(layout);
        JTable ptTable = new JTable(ptModel);
        ptTable.setTableHeader(null);
        ptPanel.setLayout(new BorderLayout());

        JScrollPane pane = new JScrollPane(ptTable);
        pane.setPreferredSize(new Dimension(400, 70));
        ptPanel.add(pane, BorderLayout.NORTH);

        ptPanel.add(new JScrollPane(ptTextArea), BorderLayout.CENTER);
        ptPanel.setBorder(new EmptyBorder(0, 0, 2, 0));
        ptTextArea.setTabSize(2);

        this.add(ptPanel);

        ctPanel.setLayout(new BorderLayout());
        JTable ctTable = new JTable(ctModel);
        ctTable.setTableHeader(null);

        pane = new JScrollPane(ctTable);
        pane.setPreferredSize(new Dimension(400, 70));
        ctPanel.add(pane, BorderLayout.NORTH);

        ctPanel.add(new JScrollPane(ctTextArea), BorderLayout.CENTER);
        ctPanel.setBorder(new EmptyBorder(2, 0, 0, 0));
        ctTextArea.setTabSize(2);

        this.add(ctPanel);
    }

    public void setCt(String text) {
        TextAnalysis textAnalisis = new TextAnalysis(text);
        ctTextArea.setText(analyze.analyse(text));
        ctModel.setFrequency(textAnalisis.getCharacterFrequency());
    }

    public void setPt(String text) {
        TextAnalysis textAnalisis = new TextAnalysis(text);
        ptTextArea.setText(analyze.analyse(text));
        ptModel.setFrequency(textAnalisis.getCharacterFrequency());
    }
}

class FrequencyTableModel extends DefaultTableModel  {
    private final int ColCount = 13;
    public FrequencyTableModel() {
        setColumnCount(ColCount);
        /*
        setRowCount(4);
        for (int i = 0; i < 13; i++) {
            setValueAt(Text.ALPHABET.charAt(i), 0, i);
        }
        for (int i = 0; i < 13; i++) {
            setValueAt(Text.ALPHABET.charAt(i + 13), 2, i);
        }
        */
    }
    public void setFrequency(java.util.List<KeyCount> frequency) {
        int rowCount = frequency.size() / ColCount;
        if (frequency.size() % ColCount != 0) {
            rowCount++;
        }

        rowCount*=2;
        setRowCount(rowCount);

        for (int row = 0; row < rowCount; row++) {
            for (int col = 0; col < ColCount; col++) {
                int pos = row/2 * ColCount + col;
                KeyCount keyCount;
                if (pos < frequency.size()) {
                    keyCount = frequency.get(pos);
                } else {
                    keyCount = new KeyCount(" ", 0);
                }

                if (row % 2 == 0) {
                    setValueAt(keyCount.getKey(), row, col);
                } else {
                    setValueAt(keyCount.getCount(), row, col);
                }
            }
        }
    }
}


package view;

import jm.cipher.Bacon;
import jm.cipher.data.IBaconData;

import javax.swing.*;
import java.awt.*;

/**
 * User: T05365A
 * Date: 30.07.12
 * Time: 15:01
 */
public class BaconPanel extends JPanel implements IBaconData {
    private JTextField alphabetTextField;
    private JTextField rangeATextField;
    private JTextField rangeBTextField;
    private JCheckBox swapCheckBox;
    private JComboBox typeCombobox;

    public String getAlphabet() {
        return alphabetTextField.getText();
    }

    public String getType() {
        return typeCombobox.getSelectedItem().toString();
    }

    public boolean getSwap() {
        return swapCheckBox.isSelected();
    }

    public String getRangeA() {
        return rangeATextField.getText();
    }

    public String getRangeB() {
        return rangeBTextField.getText();
    }

    public BaconPanel() {
        LayoutManager layout = new FlowLayout(FlowLayout.LEFT);
        this.setLayout(layout);

        this.add(new JLabel("Alphabet"));
        alphabetTextField = new JTextField(Bacon.ALPHABET, 26);
        this.add(alphabetTextField);

        typeCombobox = new JComboBox(Bacon.types.keySet().toArray());
        this.add(typeCombobox);

        swapCheckBox = new JCheckBox("Swap");
        this.add(swapCheckBox);

        this.add(new JLabel("Range"));
        rangeATextField = new JTextField(Bacon.ALPHABET.substring(0,11), 10);
        this.add(rangeATextField);
        rangeBTextField = new JTextField(Bacon.ALPHABET.substring(12), 10);
        this.add(rangeBTextField);
    }
}

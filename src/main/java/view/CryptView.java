package view;

import jm.cipher.*;
import jm.cipher.codec.NGramToAlphabet;
import jm.cipher.codes.SiebenSegmentAnzeige;
import jm.cipher.codes.StrichCode;
import jm.cipher.transposition.ColumnarTransposition;
import jm.cipher.transposition.Railfence;
import view.panels.BifidPanel;
import view.transposition.TranspositionSolverPanel;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class CryptView extends JFrame {

    private List<CipherDouble> ciphersPrimary;
    private List<CipherDouble> ciphersSecondary;
    private CryptPanel cryptPanel;
    private AnalysePanel analysePanel;
    private JPanel toolPanel;
    private JButton encryptButton = new JButton();
    private JButton decryptButton = new JButton();
    private TitledBorder titledBorder = BorderFactory.createTitledBorder("Cipher");
    private List<ViewMemento> mementos = new ArrayList<ViewMemento>();
    private int mementoPos;
    private CipherDouble currentCipherDouble;
    private JButton undoButton;
    private JButton redoButton;


    public CryptView() {
        JTabbedPane tabbedPane = new JTabbedPane();
        JPanel mainPanel = new JPanel();
        getContentPane().add(tabbedPane);

        cryptPanel = PanelFactory.getCryptPanel();
        initCiphers();
        initSecondaryCiphers();
        initializeMenu();

        decryptButton.setAction(new AbstractAction("Decrypt") {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                IPtCt ptCt = getPtCt();
                try {
                    ICipher cipher = getCurrentCipher();
                    if (cipher != null) {
                        pushMemento();
                        ptCt.setPt(cipher.decrypt(ptCt.getCt()));
                    }
                } catch (Exception e) {
                    ptCt.setPt(e.toString());
                }
            }
        });

        encryptButton.setAction(new AbstractAction("Encrypt") {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                IPtCt ptCt = cryptPanel;
                try {
                    ICipher cipher = getCurrentCipher();
	                if ("Bifid".equals(cipher.getName())) {
	                    ((BifidPanel)currentCipherDouble.getDataPanel()).putData();
	                }
                    if (cipher != null) {
                        pushMemento();
                        ptCt.setCt(cipher.encrypt(ptCt.getPt()));
                    }
                } catch (Exception e) {
                    ptCt.setCt(e.toString());
                }
            }
        });

        setTitle("Dekryptor");
        setSize(1000, 600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);


        mainPanel.setLayout(new BorderLayout());

        mainPanel.add(getToolPanel(), BorderLayout.NORTH);
        mainPanel.add(getMenuPanel(), BorderLayout.WEST);
        mainPanel.add(cryptPanel, BorderLayout.CENTER);
        analysePanel = PanelFactory.getAnalysisPanel();
        mainPanel.add(analysePanel, BorderLayout.EAST);
        cipherChanged(0);

        tabbedPane.add(mainPanel, "Main");
        tabbedPane.add(new TranspositionSolverPanel(), "ColumnarTransposition");
	    tabbedPane.add(new BrutePanel(), "Brute");
	    tabbedPane.add(new ScriptPanel(), "Script");
    }

    private ICipher getCurrentCipher() {
        if (currentCipherDouble != null) {
            return currentCipherDouble.getCipher();
        } else {
            return null;
        }
    }

    private void initCiphers() {
        ciphersPrimary = new ArrayList<>();
        ciphersPrimary.add(getPair(new ADFGX()));
        ciphersPrimary.add(getPair(new Affine()));
        ciphersPrimary.add(getPair(new AlphabetBinary()));
        ciphersPrimary.add(getPair(new AlphabetNumber()));
        ciphersPrimary.add(getPair(new AsciiBinary()));
        ciphersPrimary.add(getPair(new AsciiNumber()));
        ciphersPrimary.add(getPair(new Atbash()));
        ciphersPrimary.add(getPair(new Bacon()));
        ciphersPrimary.add(getPair(new Bifid()));
        ciphersPrimary.add(getPair(new BinaryData()));
        ciphersPrimary.add(getPair(new Braille()));
        ciphersPrimary.add(getPair(new Caesar()));
        ciphersPrimary.add(getPair(new ColumnarTransposition()));
        ciphersPrimary.add(getPair(new ConvertNumber()));
        ciphersPrimary.add(getPair(new Handy()));
        ciphersPrimary.add(getPair(new Hill()));
        ciphersPrimary.add(getPair(new MFV()));
        ciphersPrimary.add(getPair(new Morse()));
        ciphersPrimary.add(getPair(new Navajo()));
        ciphersPrimary.add(getPair(new NGramToAlphabet()));
        ciphersPrimary.add(getPair(new Playfair()));
        ciphersPrimary.add(getPair(new Polybios()));
        ciphersPrimary.add(getPair(new PSE()));
        ciphersPrimary.add(getPair(new Railfence()));
        ciphersPrimary.add(getPair(new Rsa()));
        ciphersPrimary.add(getPair(new SiebenSegmentAnzeige()));
        ciphersPrimary.add(getPair(new StrichCode()));
        ciphersPrimary.add(getPair(new Trithemius()));
        ciphersPrimary.add(getPair(new VariableBaseToInt()));
        ciphersPrimary.add(getPair(new Vigenere()));
        ciphersPrimary.add(getPair(new WordBackwards()));
        Collections.sort(ciphersPrimary);
    }

    private void initSecondaryCiphers() {
        ciphersSecondary = new ArrayList<>();
        ciphersSecondary.add(getPair(new NGramToAlphabet()));
        ciphersSecondary.add(getPair(new ConvertNumber()));
        Collections.sort(ciphersSecondary);
    }

    private CipherDouble getPair(ICipher cipher) {
        JPanel panel = PanelFactory.getDataPanel(cipher.getName());
        cipher.setIData(panel);
	    // TODO: generalize
	    if ("Bifid".equals(cipher.getName())) {
		    ((BifidPanel) panel).setData(cipher);
	    }
        return new CipherDouble(cipher, panel);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            CryptView ex = new CryptView();
            ex.setVisible(true);
        });
    }

    private JFrame getFrame() {
        return this;
    }

    private JPanel getMenuPanel() {
        JPanel panel = new JPanel(new BorderLayout());

        DefaultListModel<ICipher> model = new DefaultListModel<>();

        for (CipherDouble cipher : ciphersPrimary) {
            model.addElement(cipher.getCipher());
        }

        JList cipherJList = new JList(model) {
            public String getToolTipText(MouseEvent evt) {
                // Get item index
                int index = locationToIndex(evt.getPoint());

                // Return the tool tip text
                return ciphersPrimary.get(index).getCipher().getDescription();
            }
        };

        cipherJList.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent event) {
                if (!event.getValueIsAdjusting()) {
                    JList jList = (JList) event.getSource();
                    int index = jList.getSelectedIndex();
                    if (index >= 0 && index < ciphersPrimary.size()) {
                        cipherChanged(index);
                    }
                }
            }
        });

        panel.add(new JScrollPane(cipherJList), BorderLayout.CENTER);

        return panel;
    }

    private int selectedIndex;

    private void cipherChanged(int index) {
        selectedIndex = index;
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                CipherDouble cipherDouble = ciphersPrimary.get(selectedIndex);
                setSelectedCipherDouble(cipherDouble);
            }
        });
    }

    private void setSelectedCipherDouble(CipherDouble cipherDouble) {
        currentCipherDouble = cipherDouble;

        JPanel dataPanel = cipherDouble.getDataPanel();

        if (toolPanel.getComponentCount() == 2) {
            toolPanel.remove(1);
        }
        toolPanel.add(dataPanel, BorderLayout.CENTER);
        titledBorder.setTitle(cipherDouble.getCipher().getDisplayname());

        toolPanel.validate();
        toolPanel.repaint();
        validate();
        repaint();

        encryptButton.setEnabled(cipherDouble.getCipher().canEncrypt());
        decryptButton.setEnabled(cipherDouble.getCipher().canDecrypt());
    }

    private JPanel getToolPanel() {
        toolPanel = new JPanel(new BorderLayout());
        toolPanel.setBorder(titledBorder);
        JPanel nortPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));

        nortPanel.add(encryptButton);
        nortPanel.add(decryptButton);

        JButton reverseButton = new JButton(new AbstractAction("Reverse") {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                pushMemento();
                reverse();
            }
        });


        nortPanel.add(reverseButton);

        JButton swapButton = new JButton(new AbstractAction("Swap") {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                pushMemento();
                swapCtPt();
            }
        });

        nortPanel.add(swapButton);

        JButton analyzeButton = new JButton("Analyze");
        analyzeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                analysePanel.setCt(cryptPanel.getCt());
                analysePanel.setPt(cryptPanel.getPt());
                analysePanel.validate();
                analysePanel.repaint();
            }
        });
        nortPanel.add(analyzeButton);

        undoButton = new JButton(new AbstractAction("Undo") {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                undo();
            }
        });
        undoButton.setEnabled(false);
        nortPanel.add(undoButton);

        redoButton = new JButton(new AbstractAction("Redo") {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                redo();
            }
        });
        redoButton.setEnabled(false);
        nortPanel.add(redoButton);

        toolPanel.add(nortPanel, BorderLayout.NORTH);
        return toolPanel;
    }

    private void reverse() {
        cryptPanel.setPt(reverseLines(cryptPanel.getPt()));
        cryptPanel.setCt(reverseLines(cryptPanel.getCt()));
    }

    private String reverseLines(String pt) {
        StringBuilder pts = new StringBuilder();
        String[] lines = pt.split("\n");
        for (int i = 0; i < lines.length; i++) {
            StringBuilder line = new StringBuilder(lines[i]);
            if (i > 0) {
                pts.append("\n");
            }
            pts.append(line.reverse());
        }
        return pts.toString();
    }

    private void swapCtPt() {
        String ct = cryptPanel.getCt();
        cryptPanel.setCt(cryptPanel.getPt());
        cryptPanel.setPt(ct);
    }

    private void initializeMenu() {
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Cipher");
        menuBar.add(menu);
        menu.add(new JMenuItem(getAction(getPair(new NGramToAlphabet()))));
        menu.add(new JMenuItem(getAction(getCipherDouble("Atbash"))));
        menu.add(new JMenuItem(getAction(getCipherDouble("Bacon"))));
        menu.add(new JMenuItem(getAction(getCipherDouble("ConvertNumber"))));

        getFrame().setJMenuBar(menuBar);
    }

    private CipherDouble getCipherDouble(String ciphername) {
        for (CipherDouble cipherDouble : ciphersPrimary) {
            if (cipherDouble.getCipher().getName().equals(ciphername)) {
                return cipherDouble;
            }
        }
        for (CipherDouble cipherDouble : ciphersSecondary) {
            if (cipherDouble.getCipher().getName().equals(ciphername)) {
                return cipherDouble;
            }
        }
        return null;
    }

    private Action getAction(CipherDouble cipherDouble) {
        Action action = new AbstractAction(cipherDouble.getCipher().getDisplayname()) {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Object source = actionEvent.getSource();
                Action action = ((JMenuItem) source).getAction();
                CipherDouble cipherDouble1 = (CipherDouble) action.getValue("cipher");
                setSelectedCipherDouble(cipherDouble1);
            }
        };
        action.putValue("cipher", cipherDouble);
        return action;
    }

    private void pushMemento() {
        if (currentCipherDouble != null) {
            IPtCt ptCt = getPtCt();
            ICipher cipher = currentCipherDouble.getCipher();
            cipher.initializeData();
            removeNotNeededMementos();
            Memento memento = cipher.getMemento();
            memento.pt = ptCt.getPt();
            memento.ct = ptCt.getCt();
            mementos.add(new ViewMemento(memento, currentCipherDouble));
            mementoPos = mementos.size()-1;
            undoButton.setEnabled(mementoPos >= 0);
        }
    }

    private void removeNotNeededMementos() {
        int count = mementos.size() - 1 - mementoPos;
        for (int i = 0; i < count; i++) {
            mementos.remove(mementos.size() - 1); // remove last element
        }
    }

    private void undo() {
        if (mementos.size() > 0 && mementoPos >= 0) {
            IPtCt ptCt = getPtCt();
            ViewMemento memento = mementos.get(mementoPos);
            ICipher cipher = memento.cipherDouble.getCipher();
            cipher.setMemento(memento.memento);
            setSelectedCipherDouble(memento.cipherDouble);
            ptCt.setPt(memento.memento.pt);
            ptCt.setCt(memento.memento.ct);
            mementoPos--;
            updateButtons();
        }
    }

    private void redo() {
        mementoPos++;
        updateButtons();
        if (mementos.size() > 0 && mementoPos >= 0) {
            IPtCt ptCt = getPtCt();
            ViewMemento memento = mementos.get(mementoPos);
            ICipher cipher = memento.cipherDouble.getCipher();
            cipher.setMemento(memento.memento);
            setSelectedCipherDouble(memento.cipherDouble);
            ptCt.setPt(memento.memento.pt);
            ptCt.setCt(memento.memento.ct);
        }
    }

    private IPtCt getPtCt() {
        return cryptPanel;
    }

    private void updateButtons() {

        redoButton.setEnabled(mementoPos < (mementos.size() - 1));
        undoButton.setEnabled(mementoPos > 0);
    }
}

class ViewMemento {
    public Memento memento;
    public CipherDouble cipherDouble;

    public ViewMemento(Memento mem, CipherDouble dou) {
        memento = mem;
        cipherDouble = dou;
    }
}

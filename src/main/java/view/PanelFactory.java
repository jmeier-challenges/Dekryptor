package view;


import jm.cipher.ICipher;
import view.panels.CaesarPanel;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * User: johann
 * Date: 07.07.12
 * Time: 07:17
 */
public class PanelFactory {

    public static JPanel getShaPanel() {
        JPanel panel = new JPanel();
        LayoutManager layout = new BorderLayout();
        panel.setLayout(layout);

        JLabel label = new JLabel();
        label.setText("Test");

        JTextArea textArea = new JTextArea();
        textArea.setColumns(100);
        textArea.setText("Hello");

        panel.add(label, BorderLayout.WEST);
        panel.add(textArea, BorderLayout.CENTER);

        return panel;
    }

    public static JPanel getMenuPanel(List<ICipher> ciphers) {
        JPanel panel = new JPanel();
        LayoutManager layout = new BorderLayout();
        panel.setLayout(layout);

        DefaultListModel model = new DefaultListModel();

        for (ICipher cipher : ciphers) {
            model.addElement(cipher);
        }

        JList jlist = new JList(model);
        panel.add(jlist, BorderLayout.CENTER);

        return panel;
    }

    public static CryptPanel getCryptPanel() {

        return new CryptPanel();
    }

    public static AnalysePanel getAnalysisPanel() {
        return new AnalysePanel();
    }

    public static JPanel getDataPanel(String ciphername) {
        if ("Morse".equals(ciphername)) {
            return new MorsePanel();
        } else if ("Caesar".equals(ciphername)) {
            return new CaesarPanel();
        } else if ("CaesarBrute".equals(ciphername)) {
            return new CaesarBrutePanel();
        } else if ("AlphabetBinary".equals(ciphername)) {
            return new AlphabetBinaryPanel();
        } else if ("AlphabetNumber".equals(ciphername)) {
            return new AlphabetNumberPanel();
        } else if ("AsciiBinary".equals(ciphername)) {
            return new AsciiBinaryPanel();
        } else if ("AsciiNumber".equals(ciphername)) {
            return new AsciiNumberPanel();
        } else if ("Bacon".equals(ciphername)) {
            return new BaconPanel();
        } else if ("Playfair".equals(ciphername)) {
            return new PlayfairPanel();
        } else if ("Vigenere".equals(ciphername)) {
            return new VigenerePanel();
        } else if ("BinaryData".equals(ciphername)) {
            return new BinaryDataPanel();
        } else if ("NGramToAlphabet".equals(ciphername)) {
            return new NGramToAlphabetPanel();
        } else if ("ConvertNumber".equals(ciphername)) {
            return new ConvertNumberPanel();
        } else if ("ColumnarTransposition".equals(ciphername)) {
            return new TranspositionDataPanel();
        } else if ("VariableBaseToInt".equals(ciphername)) {
            return new VariableBaseToIntDataPanel();
        } else {
            Object panel = loadPanel(ciphername+"Panel");
            if (panel != null) {
                return (JPanel) panel;
            }
        }
        return new JPanel();
    }

    private static Object loadPanel(String classname) {
        Object panel = loadPanel(classname, "view.");
        if (panel == null) {
            panel = loadPanel(classname, "view.panels.");
        }
        if (panel == null) {
            panel = loadPanel(classname, "view.transposition.");
        }
        return panel;
    }

    private static Object loadPanel(String classname, String namespace) {

        ClassLoader classLoader = ClassLoader.getSystemClassLoader();

        try {
            Class aClass = classLoader.loadClass(namespace + classname);
            return aClass.newInstance();
        } catch (ClassNotFoundException e) {
            //e.printStackTrace();
            return null;
        } catch (InstantiationException e) {
            //e.printStackTrace();
            return null;
        } catch (IllegalAccessException e) {
            //e.printStackTrace();
            return null;
        }
    }

	private static JPanel getScriptPanel() {
		return new ScriptPanel();
	}
}

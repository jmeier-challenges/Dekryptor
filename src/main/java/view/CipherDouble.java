package view;

import jm.cipher.ICipher;

import javax.swing.*;

public class CipherDouble implements Comparable<CipherDouble> {
    private ICipher cipher;
    private JPanel dataPanel;
    private Action encryptAction = null;
    private Action decryptAction = null;

    public CipherDouble(ICipher theCipher, JPanel panel) {
        cipher = theCipher;
        dataPanel = panel;
    }

    @Override
    public int compareTo(CipherDouble cipherDouble) {
        return cipher.getDisplayname().compareTo(cipherDouble.cipher.getDisplayname());
    }

    public JPanel getDataPanel() {
        return dataPanel;
    }

    public ICipher getCipher() {
        return cipher;
    }
}
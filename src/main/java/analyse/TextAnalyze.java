package analyse;

import jm.tools.Text;
import jm.tools.TextCharacterisation;

/**
 * User: T05365A
 * Date: 31.07.12
 * Time: 08:16
 */
public class TextAnalyze {


    public String analyse(String text) {
        StringBuffer result = new StringBuffer();
        Text textTool = new Text();
        //result.append(getFrequency(text));
        //result.append("\n");
        result.append("Koinzidenzindex IC = ").append(Text.getKoinzidenzIndex(text)).append("\n");
        result.append("\n");
        result.append("Chi (de) = ").append(TextCharacterisation.chiSquareDe(text)).append("\n");
        result.append("Chi (en) = ").append(TextCharacterisation.chiSquareEn(text)).append("\n");
        result.append("Chi (fr) = ").append(TextCharacterisation.chiSquareFr(text)).append("\n");
        result.append("\n");
        result.append("Fitness, Bigram (de) = ").append(textTool.scoreDE(text)).append("\n");
        result.append("Fitness, Bigram (en) = ").append(textTool.scoreEN(text)).append("\n");

        return result.toString();
    }

    public String getFrequency(String text) {
        StringBuffer result = new StringBuffer();
        int[] frequency = Text.getCharacterFrequencies(text);
        for (char c : Text.LC_ALPHABET.toCharArray()) {
            //result.append(getPaddedString(String.valueOf(c), 5));
            result.append(c).append("\t");
        }
        result.append("\n");
        for (int f : frequency) {
            //result.append(getPaddedString(Integer.toString(f), 5));
            result.append(f).append("\t");
        }

        return result.toString();
    }

    private String getPaddedString(String s, int i) {
        String pad = "";
        int length = s.length();
        if (length < i) {
            for (int j = 0; j < i-length; j++) {
                pad += " ";
            }
        }

        return pad + s;
    }

    public static void main(String[] args) {
        TextAnalyze analyze = new TextAnalyze();
        String text = "This specification is divided into two sections. The first section, Summary, covers the basic formatting concepts. This section is intended for users who want to get started quickly and are familiar with formatted printing in other programming languages. The second section, Details, covers the specific implementation details. It is intended for users who want more precise specification of formatting behavior.";

        System.out.println(analyze.analyse(text));
    }
}
